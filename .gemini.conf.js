module.exports = {
    rootUrl: 'http://127.0.0.1:3000',
    gridUrl: 'http://127.0.0.1:4444/wd/hub',
 //   screenshotsDir: '/gemini/screens2',
   // gridUrl: 'http://selenium.example.com:4444/wd/hub',

    browsers: {
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome'
            }
        },
        /*
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                version: '47'
            },
        },
        ie11: {
            desiredCapabilities: {
                browserName: 'internet explorer',
                version: '11'
            },
        }
                */

    },
    sets:{
        ff:{
            files:[
                "src/components/*/tests/*.gemini.js"
            ]
        }
    },
    system:{
        debug: false,
        plugins: {

            babel: {
                    presets: [] // из babelrc
                },
            /*
            pluginGemini : {}
*/
/*
            react:{
                webpackConfig: "webpack/webpack.config.dev.js",
                hostname: "127.0.0.1",
                port: "5432",
                cssFiles: []
            }
*/

        }
    }
};