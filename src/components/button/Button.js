import React from 'react'


export const nextVal= oldVal=> oldVal*6;

export const loadAsyncVal =  async (oldVal) => {
    return new Promise((resolve, reject) => {
        setTimeout(()=> resolve(oldVal*3), 1000);
    })
}

const style={
    width:500,
    height:500
};
export class Button extends React.Component {
    constructor(props)
    {
        super(props);
        this.handleClick=this.handleClick.bind(this);
        this.state={value: 25}
    }

    async handleClick(){

        const oldVal=this.state.value;
        this.setState({value: "loading"});
        this.setState({value: await loadAsyncVal(oldVal)});
    }




    render() {
        return (
            <button style={style} onClick={this.handleClick} className="gjhj">{this.state.value}</button>
        );
    }
}

