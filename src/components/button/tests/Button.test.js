import React from 'react';
import {Button} from "../Button";
import renderer from 'react-test-renderer';


function sleep(time){
    return new Promise(resolve=> setTimeout(()=>resolve(), time))
}


test("Button click snapshot", async ()=>{
    const component = renderer.create(
        <Button/>
    );
    let tree=component.toJSON();
    expect(tree).toMatchSnapshot();

    tree.props.onClick();
    tree=component.toJSON();
    expect(tree).toMatchSnapshot();

 //   jest.useFakeTimers();
 //   jest.runAllTimers();
    await sleep(3000);
    tree=component.toJSON();
    expect(tree).toMatchSnapshot();

})