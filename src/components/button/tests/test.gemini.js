
gemini.suite('button', (suite) => {

    suite.setUrl('/')
        .setCaptureElements('button')
        .before((actions,find) => suite.button = find("button"))
        .capture('plain')
        .capture('firstClick', (actions) => actions.click(suite.button) )
        .capture('afterLoading', (actions) => actions.wait(3000) )
    ;
});



//https://habrahabr.ru/company/yandex/blog/238323/
//https://github.com/gemini-testing/gemini
//gemini test --reporter flat

// selenium-standalone start

/*



gemini.suite('yandex-search1', function(suite) {
    suite.setUrl('/')
        .setCaptureElements('body')
        .capture('plain')
        .capture('with text', function(actions, find) {
            actions.sendKeys(find('.input__control'), 'hello gemini');
        });
});

*/
/*
import {Button} from "../src/components/button";
import React from "react"

geminiReact.suite("react", (suite)=> {
    suite.render(<Button/>)
        .capture("first")
});
*/